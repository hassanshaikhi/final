cd /mnt/images
cat > user-data <<EOF
#cloud-config
password: $1
chpasswd: { expire: False }
ssh_pwauth: True
EOF

cloud-localds -v --network-config=network-config arvan.iso user-data 
